#pragma once

#include "Response.h"
#include "Helper.h"

class SumTotalResponse : public Framework::Response
{
public:
	SumTotalResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};