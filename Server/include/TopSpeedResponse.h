#pragma once

#include "Response.h"
#include "Helper.h"

class TopSpeedResponse : public Framework::Response
{
public:
	TopSpeedResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};