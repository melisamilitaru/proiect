#pragma once
#include <string>
#include<iostream>

class Pokemon
{
public:
	Pokemon(int id, std::string name, std::string type1, std::string type2, int total, int hp, int attack, int defense, int spAtk, int spDef, int speed);

	int id, total, hp, attack, defense, spAtk, spDef, speed;
	std::string name, type1, type2;
};