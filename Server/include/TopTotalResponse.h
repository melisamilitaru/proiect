#pragma once

#include "Response.h"
#include "Helper.h"

class TopTotalResponse : public Framework::Response
{
public:
	TopTotalResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};