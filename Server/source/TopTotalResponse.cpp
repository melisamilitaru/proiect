#include "TopTotalResponse.h"

TopTotalResponse::TopTotalResponse() : Response("TopTotal")
{

}

std::string TopTotalResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	Helper helper;
	std::vector<Pokemon> pokemons = helper.read();
	std::vector<std::pair<int, std::string>> nameTotal;
	std::vector<int> total;
	std::vector<std::string> names;
	for each(auto pokemon in pokemons)
	{
		total.push_back(pokemon.total);
		names.push_back(pokemon.name);
	}
	for (int index = 0; index < total.size(); index++)
	{
		nameTotal.push_back(std::make_pair(total[index], names[index]));
	}
	std::sort(nameTotal.begin(), nameTotal.end(), [](auto &left, auto &right) {
		return left.first > right.first;
	});
	this->content.push_back(boost::property_tree::ptree::value_type("File", "topTotal.txt"));
	for (int index = 0; index < 10; index++)
	{
		this->content.push_back(boost::property_tree::ptree::value_type(nameTotal[index].second, std::string(std::to_string(nameTotal[index].first))));
	}


	return this->getContentAsString();
}
