#include "SumTotalResponse.h"
#include <unordered_map>
#include <iterator>
#include <boost/range/numeric.hpp>

SumTotalResponse::SumTotalResponse() : Response("SumTotal")
{

}

std::string SumTotalResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	Helper helper;
	std::vector<Pokemon> pokemons = helper.read();
	std::unordered_map<std::string, int> types;

	for each(auto pokemon in pokemons)
	{
		if (pokemon.type1.compare(pokemon.type2)==0)
		{
			std::unordered_map<std::string, int>::iterator it = types.find(pokemon.type1);
			if (it != types.end()) {
				it->second += pokemon.total;
			}
			else {
				types.insert(std::make_pair(pokemon.type1, pokemon.total));
			}
		}
	}
	this->content.push_back(boost::property_tree::ptree::value_type("File", "sumTotal.txt"));
	for (auto& element : types)
		this->content.push_back(boost::property_tree::ptree::value_type(element.first, std::string(std::to_string(element.second))));

	return this->getContentAsString();
}