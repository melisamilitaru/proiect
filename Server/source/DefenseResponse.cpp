#include "DefenseResponse.h"

DefenseResponse::DefenseResponse() : Response("Defense")
{

}

std::string DefenseResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	Helper helper;
	std::vector<Pokemon> pokemons = helper.read();
	std::vector<std::pair<int, std::string>> nameDefense;
	std::vector<int> defense;
	std::vector<std::string> names;
	for each(auto pokemon in pokemons)
	{
		defense.push_back(pokemon.defense);
		names.push_back(pokemon.name);
	}
	for (int index = 0; index < defense.size(); index++)
	{
		if (defense[index] > 0 && defense[index] < 20)
			nameDefense.push_back(std::make_pair(defense[index], names[index]));
	}
	this->content.push_back(boost::property_tree::ptree::value_type("File", "defense.txt"));
	for (int index = 0; index < nameDefense.size(); index++)
		this->content.push_back(boost::property_tree::ptree::value_type(nameDefense[index].second, std::string(std::to_string(nameDefense[index].first))));

	return this->getContentAsString();
}
